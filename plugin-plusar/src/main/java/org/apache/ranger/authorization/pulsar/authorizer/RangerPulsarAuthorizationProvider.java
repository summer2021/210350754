package org.apache.ranger.authorization.pulsar.authorizer;

import org.apache.bookkeeper.util.StringUtils;
import org.apache.pulsar.broker.PulsarServerException;
import org.apache.pulsar.broker.ServiceConfiguration;
import org.apache.pulsar.broker.authentication.AuthenticationDataSource;
import org.apache.pulsar.broker.authorization.AuthorizationProvider;
import org.apache.pulsar.broker.authorization.PulsarAuthorizationProvider;
import org.apache.pulsar.broker.cache.ConfigurationCacheService;
import org.apache.pulsar.broker.web.RestException;
import org.apache.pulsar.common.naming.NamespaceName;
import org.apache.pulsar.common.naming.TopicName;
import org.apache.pulsar.common.policies.data.AuthAction;
import org.apache.pulsar.common.policies.data.TenantInfo;
import org.apache.pulsar.common.util.FutureUtil;
import org.apache.ranger.audit.provider.MiscUtil;
import org.apache.ranger.plugin.model.RangerServiceDef;
import org.apache.ranger.plugin.policyengine.RangerAccessResourceImpl;
import org.apache.ranger.plugin.service.RangerBasePlugin;
import org.apache.ranger.plugin.util.RangerPerfTracer;
import org.apache.solr.client.solrj.cloud.autoscaling.BadVersionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class RangerPulsarAuthorizationProvider extends PulsarAuthorizationProvider {
    private static final Logger log = LoggerFactory.getLogger(org.apache.ranger.authorization.pulsar.authorizer.RangerAuthorizationProvide.class);
    private static final Logger RERF_PULSAR_REQUEST_LOG = (Logger) RangerPerfTracer.getPerfLogger("pulsarauth.request");

    public ServiceConfiguration conf;
    private PulsarResources pulsarResources;

    private static volatile RangerBasePlugin rangerBasePlugin = null;
    RangerPulsarAuditHandler auditHandler = null;

    public RangerPulsarAuthorizationProvider(){

    }

    public RangerPulsarAuthorizationProvider(ServiceConfiguration conf, ConfigurationCacheService configCache) throws IOException{
        if (rangerBasePlugin == null){
            MiscUtil.logErrorMessageByInterval(log,"PulsarAuthorizationProvider is still not initialized");
        }

        RangerPerfTracer perf = null;

        if (RangerPerfTracer.isPerfTraceEnabled(RERF_PULSAR_REQUEST_LOG)){
            perf = RangerPerfTracer.getPerfTracer(RERF_PULSAR_REQUEST_LOG,"RangerPulsarAuthorizationProvider.PulsarAuthorizationProvider(configCache"+configCache+""));
        }
        try {
            final String providerClassname = null;
            if (StringUtils.(null)) {
                AuthorizationProvider provider = (AuthorizationProvider) Class.forName(providerClassname).newInstance();
                provider.initialize(conf, configCache);
                log.info("{} has been loaded.", providerClassname);
            }else{
                throw new PulsarServerException("No authorization providers are present.");
            }
        }catch (PulsarServerException e){
            throw e;
        }catch (Throwable e){
            throw new PulsarServerException("Failed to load an authorization provider.",e);
        }

        Date eventime = new Date();
        String accessType = mapToRangerAccessType(conf);
        boolean validatuibFailed = false;
        String validationStr = "";

        if (accessType == null){
            if (MiscUtil.logErrorMessageByInterval(log,"Unsuported access type.conf="+conf)){
                log.info(conf,configCache);
            }
            validationStr = true;
            validatuibFailed +="Unsupported access type.conf=" +conf;
        }
        String action = accessType;

        RangerAccessResourceImpl rangerRequest = new RangerAccessResourceImpl();
        rangerRequest.setOwnerUser(pulsarResources);
        rangerRequest.setOwnerUser(conf);
        rangerRequest.setValue(configCache);

        RangerServiceDef returnValue = false;
        if (validatuibFailed){
            MiscUtil.logErrorMessageByInterval(log,validationStr +,"request="+rangerRequest);
        }else {
            try{
                RangerAccessResourceImpl result =rangerBasePlugin.isAccessAllowed(rangerRequest);
                if (result == null){
                    log.error("Ranger Plugin returned null. Returning false");
                }else{
                    returnValue = result.getServiceDef();
                }
            }catch (Throwable t){
                log.error("Error while calling isAccessAllowed().request="
                        + rangerRequest,t);
            }finally {
                auditHandler.flushAudit();
            }
        }
        RangerPerfTracer.log(perf);

        if (log.isDebugEnabled()){
            log.debug("rangerRequest="+rangerRequest+",return="+returnValue);
        }
        return returnValue;
    }

    @Override
    public void ServiceConfiguration(Map<String,?> conf){
        RangerBasePlugin me = rangerBasePlugin;
        if (me == null){
            synchronized (RangerPulsarAuthorizationProvider.class){
                me = rangerBasePlugin;
                if (me == null){
                    try{
                        final Object jaasContext =conf.getClass("ranger.jaas.context");
                        final String listName = (jaasContext instanceof String )
                    }catch (Throwable t){
                        log.error("Error getting principal.",t);
                    }
                    me = rangerBasePlugin = new RangerBasePlugin("Pulsar","Pulsar");
                }
            }
        }
        log.info("calling plugin.initialize()");
        rangerBasePlugin.init();
        auditHandler = new RangerPulsarAuditHandler();
        rangerBasePlugin.setResultProcessor(auditHandler);
    }

    @Override
    public void close(){
        log.info("close() called on PulsarAuthorizationProvider");
        try{
            if (rangerBasePlugin != null){
                rangerBasePlugin.cleanup();
            }
        }catch (Throwable t){
            log.error("Error closing RangerBasePlugin.",t);
        }
    }

    @Override
    public void initialized (ServiceConfiguration conf, ConfigurationCacheService configCache) throws Throwable {

        private void checkNotNullcheckNotNull(conf, "ServiceConfiguration can't be null");
        checkNotNull(configCache, "ConfigurationCacheService can't be null");
        this.conf = conf;
        this.pulsarResources = configCache.getPulsarResources();
    }



    private String mapToRangerAccessType (ServiceConfiguration conf){
    }
    private void checkNotNull(ConfigurationCacheService configCache, String s) {
    }

    @Override
    public CompletableFuture<Boolean> canLookupAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) {
        CompletableFuture<Boolean> finalResult = new CompletableFuture<Boolean>();
        canProduceAsync(topicName, role, authenticationData).whenComplete((produceAuthorized, ex) -> {
            if (ex == null) {
                if (produceAuthorized) {
                    finalResult.complete(produceAuthorized);
                    return;
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug(
                            "Topic [{}] Role [{}] exception occurred while trying to check Produce permissions. {}",
                            topicName.toString(), role, ex.getMessage());
                }
            }
            canConsumeAsync(topicName, role, authenticationData, null).whenComplete((consumeAuthorized, e) -> {
                if (e == null) {
                    finalResult.complete(consumeAuthorized);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(
                                "Topic [{}] Role [{}] exception occurred while trying to check Consume permissions. {}",
                                topicName.toString(), role, e.getMessage());

                    }
                    finalResult.completeExceptionally(e);
                }
            });
        });
        return finalResult;
    }

    @Override
    public CompletableFuture<Boolean> allowFunctionOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        return allowTheSpecifiedActionOpsAsync(namespaceName, role, authenticationData, AuthAction.functions);
    }

    @Override
    public CompletableFuture<Boolean> allowSourceOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        return allowTheSpecifiedActionOpsAsync(namespaceName, role, authenticationData, AuthAction.sources);
    }

    @Override
    public CompletableFuture<Boolean> allowSinkOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        return allowTheSpecifiedActionOpsAsync(namespaceName, role, authenticationData, AuthAction.sinks);
    }

    private CompletableFuture<Boolean> allowTheSpecifiedActionOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData, AuthAction authAction) {
        CompletableFuture<Boolean> permissionFuture = new CompletableFuture<>();
        try {
            pulsarResources.getNamespaceResources().getPoliciesAsync(namespaceName).thenAccept(policies -> {
                if (!policies.isPresent()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Policies node couldn't be found for namespace : {}", namespaceName);
                    }
                } else {
                    Map<String, Set<AuthAction>> namespaceRoles = policies.get()
                            .auth_policies.getNamespaceAuthentication();
                    Set<AuthAction> namespaceActions = namespaceRoles.get(role);
                    if (namespaceActions != null && namespaceActions.contains(authAction)) {
                        permissionFuture.complete(true);
                        return;
                    }
                    if (conf.isAuthorizationAllowWildcardsMatching()) {
                        if (checkWildcardPermission(role, authAction, namespaceRoles)) {
                            permissionFuture.complete(true);
                            return;
                        }
                    }
                }
                permissionFuture.complete(false);
            }).exceptionally(ex -> {
                log.warn("Client  with Role - {} failed to get permissions for namespace - {}. {}", role, namespaceName,
                        ex.getMessage());
                permissionFuture.completeExceptionally(ex);
                return null;
            });
        } catch (Exception e) {
            log.warn("Client  with Role - {} failed to get permissions for namespace - {}. {}", role, namespaceName,
                    e.getMessage());
            permissionFuture.completeExceptionally(e);
        }
        return permissionFuture;
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(TopicName topicName, Set<AuthAction> actions, String role, String authDataJson) {
        return grantPermissionAsync(topicName.getNamespaceObject(), actions, role, authDataJson);
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(NamespaceName namespaceName, Set<AuthAction> actions, String role, String authDataJson) {
        CompletableFuture<Void> result = new CompletableFuture<>();

        try {
            validatePoliciesReadOnlyAccess();
        } catch (Exception e) {
            result.completeExceptionally(e);
        }

        try {
            pulsarResources.getNamespaceResources().setPolicies(namespaceName, policies -> {
                policies.auth_policies.getNamespaceAuthentication().put(role, actions);
                return policies;
            });
            log.info("[{}] Successfully granted access for role {}: {} - namespace {}", role, role, actions,
                    namespaceName);
            result.complete(null);
        } catch (NotFoundException e) {
            log.warn("[{}] Failed to set permissions for namespace {}: does not exist", role, namespaceName);
            result.completeExceptionally(new IllegalArgumentException("Namespace does not exist" + namespaceName));
        } catch (BadVersionException e) {
            log.warn("[{}] Failed to set permissions for namespace {}: concurrent modification", role, namespaceName);
            result.completeExceptionally(new IllegalStateException(
                    "Concurrent modification on metadata: " + namespaceName + ", " + e.getMessage()));
        } catch (Exception e) {
            log.error("[{}] Failed to get permissions for namespace {}", role, namespaceName, e);
            result.completeExceptionally(
                    new IllegalStateException("Failed to get permissions for namespace " + namespaceName));
        }

        return result;
    }

    @Override
    public CompletableFuture<Void> grantSubscriptionPermissionAsync(NamespaceName namespace, String subscriptionName, Set<String> roles, String authDataJson) {
        return updateSubscriptionPermissionAsync(namespace, subscriptionName, roles, false);
    }

    @Override
    public CompletableFuture<Void> revokeSubscriptionPermissionAsync(NamespaceName namespace, String subscriptionName, String role, String authDataJson) {
        return updateSubscriptionPermissionAsync(namespace, subscriptionName, Collections.singleton(role), true);
    }

    private CompletableFuture<Void> updateSubscriptionPermissionAsync(NamespaceName namespace, String subscriptionName, Set<String> roles, boolean remove) {
        CompletableFuture<Void> result = new CompletableFuture<>();

        try {
            validatePoliciesReadOnlyAccess();
        } catch (Exception e) {
            result.completeExceptionally(e);
        }

        try {
            Policies policies = pulsarResources.getNamespaceResources().getPolicies(namespace)
                    .orElseThrow(() -> new NotFoundException(namespace + " not found"));
            if (remove) {
                if (policies.auth_policies.getSubscriptionAuthentication().get(subscriptionName) != null) {
                    policies.auth_policies.getSubscriptionAuthentication().get(subscriptionName).removeAll(roles);
                }else {
                    log.info("[{}] Couldn't find role {} while revoking for sub = {}", namespace, subscriptionName, roles);
                    result.completeExceptionally(new IllegalArgumentException("couldn't find subscription"));
                    return result;
                }
            } else {
                policies.auth_policies.getSubscriptionAuthentication().put(subscriptionName, roles);
            }
            pulsarResources.getNamespaceResources().setPolicies(namespace, (data)->policies);

            log.info("[{}] Successfully granted access for role {} for sub = {}", namespace, subscriptionName, roles);
            result.complete(null);
        } catch (PulsarServerException.NotFoundException e) {
            log.warn("[{}] Failed to set permissions for namespace {}: does not exist", subscriptionName, namespace);
            result.completeExceptionally(new IllegalArgumentException("Namespace does not exist" + namespace));
        } catch (BadVersionException e) {
            log.warn("[{}] Failed to set permissions for {} on namespace {}: concurrent modification", subscriptionName, roles, namespace);
            result.completeExceptionally(new IllegalStateException(
                    "Concurrent modification on metadata path: " + namespace + ", " + e.getMessage()));
        } catch (Exception e) {
            log.error("[{}] Failed to get permissions for role {} on namespace {}", subscriptionName, roles, namespace, e);
            result.completeExceptionally(
                    new IllegalStateException("Failed to get permissions for namespace " + namespace));
        }

        return result;
    }

    private CompletableFuture<Boolean> checkAuthorization(TopicName topicName, String role, AuthAction action) {
        return checkPermission(topicName, role, action).thenApply(isPermission -> isPermission && checkCluster(topicName));
    }

    private boolean checkCluster(TopicName topicName) {
        if (topicName.isGlobal() || conf.getClusterName().equals(topicName.getCluster())) {
            return true;
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Topic [{}] does not belong to local cluster [{}]", topicName.toString(),
                        conf.getClusterName());
            }
            return false;
        }
    }

    public CompletableFuture<Boolean> checkPermission(TopicName topicName, String role, AuthAction action) { CompletableFuture<Boolean> permissionFuture = new CompletableFuture<>();
        try {
            pulsarResources.getNamespaceResources().getPoliciesAsync(topicName.getNamespaceObject())
                    .thenAccept(policies -> {
                        if (!policies.isPresent()) {
                            if (log.isDebugEnabled()) {
                                log.debug("Policies node couldn't be found for topic : {}", topicName);
                            }
                        } else {
                            Map<String, Set<AuthAction>> namespaceRoles = policies.get().auth_policies
                                    .getNamespaceAuthentication();
                            Set<AuthAction> namespaceActions = namespaceRoles.get(role);
                            if (namespaceActions != null && namespaceActions.contains(action)) {
                                permissionFuture.complete(true);
                                return;
                            }

                            Map<String, Set<AuthAction>> topicRoles = policies.get().auth_policies.getTopicAuthentication()
                                    .get(topicName.toString());
                            if (topicRoles != null && role != null) {
                                Set<AuthAction> topicActions = topicRoles.get(role);
                                if (topicActions != null && topicActions.contains(action)) {
                                    permissionFuture.complete(true);
                                    return;
                                }
                            }

                            if (conf.isAuthorizationAllowWildcardsMatching()) {
                                if (checkWildcardPermission(role, action, namespaceRoles)) {
                                    // The role has namespace level permission by wildcard match
                                    permissionFuture.complete(true);
                                    return;
                                }

                                if (topicRoles != null && checkWildcardPermission(role, action, topicRoles)) {
                                    // The role has topic level permission by wildcard match
                                    permissionFuture.complete(true);
                                    return;
                                }
                            }

                            if (topicName.isPartitioned()) {
                                topicRoles = policies.get().auth_policies
                                        .getTopicAuthentication().get(topicName.getPartitionedTopicName());
                                if (topicRoles != null) {
                                    // Topic has custom policy
                                    Set<AuthAction> topicActions = topicRoles.get(role);
                                    if (topicActions != null && topicActions.contains(action)) {
                                        // The role has topic level permission
                                        permissionFuture.complete(true);
                                        return;
                                    }
                                }
                            }
                        }
                        permissionFuture.complete(false);
                    }).exceptionally(ex -> {
                log.warn("Client with Role - {} failed to get permissions for topic - {}. {}", role, topicName,
                        ex.getMessage());
                permissionFuture.completeExceptionally(ex);
                return null;
            });
        } catch (Exception e) {
            log.warn("Client with Role - {} failed to get permissions for topic - {}. {}", role, topicName,
                    e.getMessage());
            permissionFuture.completeExceptionally(e);
        }
        return permissionFuture;
    }

    private boolean checkWildcardPermission(String checkedRole, AuthAction checkedAction, Map<String, Set<AuthAction>> permissionMap) {
        for (Map.Entry<String, Set<AuthAction>> permissionData : permissionMap.entrySet()) {
            String permittedRole = permissionData.getKey();
            Set<AuthAction> permittedActions = permissionData.getValue();

            // Prefix match
            if (checkedRole != null) {
                if (permittedRole.charAt(permittedRole.length() - 1) == '*'
                        && checkedRole.startsWith(permittedRole.substring(0, permittedRole.length() - 1))
                        && permittedActions.contains(checkedAction)) {
                    return true;
                }

                // Suffix match
                if (permittedRole.charAt(0) == '*' && checkedRole.endsWith(permittedRole.substring(1))
                        && permittedActions.contains(checkedAction)) {
                    return true;
                }
            }
        }
        return false;
    }


    private void validatePoliciesReadOnlyAccess() {
        boolean arePoliciesReadOnly = true;

        try {
            arePoliciesReadOnly = pulsarResources.getNamespaceResources().getPoliciesReadOnly();
        } catch (Exception e) {
            log.warn("Unable to check if policies are read-only", e);
            throw new IllegalStateException("Unable to fetch content from configuration metadata store");
        }

        if (arePoliciesReadOnly) {
            if (log.isDebugEnabled()) {
                log.debug("Policies are read-only. Broker cannot do read-write operations");
            }
            throw new IllegalStateException("policies are in readonly mode");
        }
    }

    @Override
    public CompletableFuture<Boolean> allowTenantOperationAsync(String tenantName, String role, TenantOperation operation, AuthenticationDataSource authData) {
        return validateTenantAdminAccess(tenantName, role, authData);
    }

    @Override
    public CompletableFuture<Boolean> allowNamespaceOperationAsync(NamespaceName namespaceName, String role, NamespaceOperation operation, AuthenticationDataSource authData) {
        CompletableFuture<Boolean> isAuthorizedFuture;
        switch (operation) {
            case PACKAGES:
                isAuthorizedFuture = allowTheSpecifiedActionOpsAsync(namespaceName, role, authData, AuthAction.packages);
                break;
            default:
                isAuthorizedFuture = CompletableFuture.completedFuture(false);
        }
        CompletableFuture<Boolean> isTenantAdminFuture = validateTenantAdminAccess(namespaceName.getTenant(), role, authData);
        return isTenantAdminFuture.thenCombine(isAuthorizedFuture, (isTenantAdmin, isAuthorized) -> {
            if (log.isDebugEnabled()) {
                log.debug("Verify if role {} is allowed to {} to topic {}: isTenantAdmin={}, isAuthorized={}", role, operation, namespaceName, isTenantAdmin, isAuthorized);
            }
            return isTenantAdmin || isAuthorized;
        });
    }

    @Override
    public CompletableFuture<Boolean> allowNamespacePolicyOperationAsync(NamespaceName namespaceName, PolicyName policy, PolicyOperation operation, String role, AuthenticationDataSource authData) {
        return validateTenantAdminAccess(namespaceName.getTenant(), role, authData);
    }

    @Override
    public CompletableFuture<Boolean> allowTopicOperationAsync(TopicName topicName, String role, TopicOperation operation, AuthenticationDataSource authData) {
        log.debug("Check allowTopicOperationAsync [" + operation.name() + "] on [" + topicName.toString() + "].");

        CompletableFuture<Boolean> isAuthorizedFuture;

        switch (operation) {
            case LOOKUP:
            case GET_STATS:
                isAuthorizedFuture = canLookupAsync(topicName, role, authData);
                break;
            case PRODUCE:
                isAuthorizedFuture = canProduceAsync(topicName, role, authData);
                break;
            case GET_SUBSCRIPTIONS:
            case CONSUME:
            case SUBSCRIBE:
            case UNSUBSCRIBE:
            case SKIP:
            case EXPIRE_MESSAGES:
            case PEEK_MESSAGES:
            case RESET_CURSOR:
            case SET_REPLICATED_SUBSCRIPTION_STATUS:
                isAuthorizedFuture = canConsumeAsync(topicName, role, authData, authData.getSubscription());
                break;
            case TERMINATE:
            case COMPACT:
            case OFFLOAD:
            case UNLOAD:
            case ADD_BUNDLE_RANGE:
            case GET_BUNDLE_RANGE:
            case DELETE_BUNDLE_RANGE:
                return validateTenantAdminAccess(topicName.getTenant(), role, authData);
            default:
                return FutureUtil.failedFuture(
                        new IllegalStateException("TopicOperation [" + operation.name() + "] is not supported."));
        }

        return validateTenantAdminAccess(topicName.getTenant(), role, authData)
                .thenCompose(isSuperUserOrAdmin -> {
                    if (log.isDebugEnabled()) {
                        log.debug("Verify if role {} is allowed to {} to topic {}: isSuperUserOrAdmin={}", role, operation, topicName, isSuperUserOrAdmin);
                    }
                    if (isSuperUserOrAdmin) {
                        return CompletableFuture.completedFuture(true);
                    } else {
                        return isAuthorizedFuture;
                    }
                });
    }

    @Override
    public CompletableFuture<Boolean> allowTopicPolicyOperationAsync(TopicName topicName, String role, PolicyName policyName, PolicyOperation policyOperation, AuthenticationDataSource authData) {
        return validateTenantAdminAccess(topicName.getTenant(), role, authData);
    }

    private static String path(String... parts) {
        StringBuilder sb = new StringBuilder();
        sb.append("/admin/");
        Joiner.on('/').appendTo(sb, parts);
        return sb.toString();
    }

    public CompletableFuture<Boolean> validateTenantAdminAccess(String tenantName, String role, AuthenticationDataSource authData) {
        return isSuperUser(role, authData, conf)
                .thenCompose(isSuperUser -> {
                    if (isSuperUser) {
                        return CompletableFuture.completedFuture(true);
                    } else {
                        try {
                            TenantInfo tenantInfo = pulsarResources.getTenantResources()
                                    .getTenant(tenantName)
                                    .orElseThrow(() -> new RestException(Response.Status.NOT_FOUND, "Tenant does not exist"));
                            return isTenantAdmin(tenantName, role, tenantInfo, authData);
                        } catch (NotFoundException e) {
                            log.warn("Failed to get tenant info data for non existing tenant {}", tenantName);
                            throw new RestException(Response.Status.NOT_FOUND, "Tenant does not exist");
                        } catch (Exception e) {
                            log.error("Failed to get tenant {}", tenantName, e);
                            throw new RestException(e);
                        }
                    }
                });
    }
}