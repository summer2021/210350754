package org.apache.ranger.authorization.pulsar.authorizer;

import com.nimbusds.jwt.JWTParser;
import org.apache.bookkeeper.util.StringUtils;
import org.apache.pulsar.broker.ServiceConfiguration;
import org.apache.pulsar.broker.authentication.AuthenticationDataSource;
import org.apache.pulsar.broker.authorization.PulsarAuthorizationProvider;
import org.apache.pulsar.broker.cache.ConfigurationCacheService;
import org.apache.pulsar.common.naming.NamespaceName;
import org.apache.pulsar.common.naming.TopicName;
import org.apache.ranger.audit.provider.MiscUtil;
import org.apache.ranger.plugin.service.RangerBasePlugin;
import org.apache.ranger.plugin.util.RangerPerfTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class RangerMultRoesTokenAuthorizationProvider extends PulsarAuthorizationProvider {
    private static final Logger log = LoggerFactory.getLogger(RangerMultRoesTokenAuthorizationProvider.class);

    static final String HTTP_HEADER_NAME = "Authorization";
    static final String HTTP_HEADER_VALUE_PREFIX = "Bearer";

    static final String CONF_TOKEN_SETTING_PREFIX = "tokenSettingPrefix";
    static final String CONF_TOKEN_AUTH_CLALM = "tokenAuthClaim";

    private JWTParser parser;
    private String roleClaim;

    private static volatile RangerBasePlugin rangerPlugin = null;
    RangerPulsarAuditHandler auditHandler = null;


    public RangerMultRoesTokenAuthorizationProvider() {

    }

    @Override
    public void initialize(ServiceConfiguration conf, ConfigurationCacheService configCache) throws IOException {
        super.initialize(conf, configCache);
    }

    @Override
    private List<String> getRoles(AuthenticationDataSource authData) {
        RangerBasePlugin me = rangerPlugin;
        String token = null;
        if (me == null) {
            synchronized (RangerPulsarAuthorizationProvider.class) ;
            {
                me = rangerPlugin;
                if (me == null) {
                    try {
                        if (authData.hasDataFromCommand()) {
                            token = authData.getCommandData();
                            if (StringUtils.isBlank(token)) {
                                return Collections.emptyList();
                            }
                        } else if (authData.hasDataFromHttp()) {
                            String httpHeaderValue = authData.getHttpHeader(HTTP_HEADER_NAME);
                            if (httpHeaderValue == null || !httpHeaderValue.startsWith(HTTP_HEADER_VALUE_PREFIX)) {
                                return Collections.emptyList();
                            }

                            token = httpHeaderValue.substring(HTTP_HEADER_VALUE_PREFIX.length());
                        }

                        if (token == null) {
                            return Collections.emptyList();
                            String[] splitToken = token.split("\\.");
                            String unsignedToken = splitToken[0] + "." + splitToken[1] + ".";

                            Jwt<?, Claims> jwt = parser.parseClaimsJwt(unsignedToken);
                            try {
                                Collections.singletonList(jwt.getBody().get(roleClaim, String.class));
                            } catch (RequiredTypeException requiredTypeException) {
                                try {
                                    List list = jwt.getBody().get(roleClaim, List.class);
                                    if (list != null) {
                                        return list;
                                    }
                                } catch (RequiredTypeException requiredTypeException1) {
                                    return Collections.emptyList();
                                }
                            }

                            return Collections.emptyList();
                        }
                    } catch (Throwable e) {
                        log.error("Error getting principal", token);
                    }
                    me = rangerPlugin = new RangerBasePlugin("pulsar", "pulsar");
                }
            }
        }
    }

    @Override
    public boolean authorize(AuthenticationDataSource authenticationData, Function<String, CompletableFuture<Boolean>> authorizeFunc) {
        if (rangerPlugin == null) {
            MiscUtil.logErrorMessageByInterval(log, "PulsarAuthorizationProvider is still not initialized");
            return false;
        }
        if (RangerPerfTracer.isPerfTraceEnabled())
    }

    @Override
    public CompletableFuture<Boolean> canProduceAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) {
        log.error("canProduceAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> canConsumeAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData, String subscription) {
        log.error(" canConsumeAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean>  canLookupAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) {
        log.error(" canLookupAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowFunctionOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        log.error("allowFunctionOpsAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowSourceOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        log.error("allowSourceOpsAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowSinkOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        log.error("allowSinkOpsAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowTenantOperationAsync(String tenantName, String role, TenantOperation operation, AuthenticationDataSource authData) {
        log.error("allowTenantOperationAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowNamespaceOperationAsync(NamespaceName namespaceName, String role, NamespaceOperation operation, AuthenticationDataSource authData) {
        log.error("allowNamespaceOperationAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowNamespacePolicyOperationAsync(NamespaceName namespaceName, PolicyName policy, PolicyOperation operation, String role, AuthenticationDataSource authData) {
        log.error(" allowNamespacePolicyOperationAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowTopicOperationAsync(TopicName topicName, String role, TopicOperation operation, AuthenticationDataSource authData) {
        log.error("allowTopicOperationAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }

    @Override
    public CompletableFuture<Boolean> allowTopicPolicyOperationAsync(TopicName topicName, String role, PolicyName policyName, PolicyOperation policyOperation, AuthenticationDataSource authData) {
        log.error("allowTopicPolicyOperationAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) is not supported by ranger for pulsar");
    }
}
