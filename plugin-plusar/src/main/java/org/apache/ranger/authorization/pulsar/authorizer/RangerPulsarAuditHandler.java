package org.apache.ranger.authorization.pulsar.authorizer;

import org.apache.ranger.audit.model.AuthzAuditEvent;
import org.apache.ranger.plugin.audit.RangerDefaultAuditHandler;
import org.apache.ranger.plugin.policyengine.RangerAccessRequest;
import org.apache.ranger.plugin.policyengine.RangerAccessResourceImpl;
import org.apache.ranger.plugin.policyengine.RangerAccessResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RangerPulsarAuditHandler extends RangerDefaultAuditHandler {
    private static final Logger log = LoggerFactory.getLogger(RangerPulsarAuditHandler.class);

    private AuthzAuditEvent auditEvent = null;

    public RangerPulsarAuditHandler(){

    }

    @Override
    public void processResult(RangerAccessResult result){
        if (!isAuditingNeeded(result)){
            return;
        }
        auditEvent = super.getAuthzEvents(result);
    }

    private boolean isAuditingNeeded(RangerAccessResult result) {
        boolean ret = true;
        boolean isAllowed = result.getIsAllowed();
        RangerAccessRequest request = result.getAccessRequest();
        RangerAccessResourceImpl resource = (RangerAccessResourceImpl)request.getResource();
        String resourceName = (String)resource.getValue(String.valueOf(RangerPulsarAuthorizationProvider.class));
        if (resourceName != null){
            if (request.getAccessType().equalsIgnoreCase(String.valueOf(RangerPulsarAuthorizationProvider.class)) && !isAllowed){
                ret = false;
            }
        }
    }

    public void flushAudit(){
        if (log.isDebugEnabled()){
            log.info("==> RangerYarnAuditHandler.flushAudit(" + "AuditEvent: " + auditEvent + ")");
        }
        if (auditEvent != null){
            super.logAuthzAudit(auditEvent);
        }
        if (log.isDebugEnabled()){
            log.info("<== RangerYarnAuditHandler.flushAudit(" + "AuditEvent: " + auditEvent + ")");
        }
    }

}
