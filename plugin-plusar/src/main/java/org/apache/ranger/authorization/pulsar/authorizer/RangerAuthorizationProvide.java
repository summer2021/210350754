package org.apache.ranger.authorization.pulsar.authorizer;

import com.sun.xml.internal.ws.policy.EffectiveAlternativeSelector;
import org.apache.kafka.common.security.auth.KafkaPrincipal;
import org.apache.pulsar.broker.authorization.AuthorizationProvider;
import org.apache.pulsar.common.naming.NamespaceName;
import org.apache.pulsar.common.naming.TopicName;
import org.apache.pulsar.broker.ServiceConfiguration;
import org.apache.pulsar.broker.authentication.AuthenticationDataSource;
import org.apache.pulsar.broker.cache.ConfigurationCacheService;
import org.apache.pulsar.common.policies.data.AuthAction;
import org.apache.pulsar.shade.org.eclipse.util.resource.Resource;
import org.apache.ranger.plugin.classloader.RangerPluginClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.io.IOException;
import java.security.acl.Acl;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class RangerAuthorizationProvide implements AuthorizationProvider {

    private static final Logger log = LoggerFactory.getLogger(RangerAuthorizationProvide.class);

    private static final String RANGER_PLUGIN_TYPE = "pulsar";
    private static final String RANGER_PULASR_AUTHORIZER_IMPL_CLASSNAME = "org.apache.ranger.authorization.pulsar.authorizer.RangerAuthorizationProvide";

    private AuthAction rangerPulsarAuthorozerImpl = null;
    private static RangerPluginClassLoader rangerPluginClassLoader = null;


    public RangerAuthorizationProvide() {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.RangerAuthorizationProvide()");
        }

        this.initialize();

        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.RangerAuthorizationProvide()");
        }
    }

    private void initialize() {
    }

    public void initialize(ServiceConfiguration serviceConfiguration, ConfigurationCacheService configurationCacheService) throws IOException {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.initialize()");
        }

        try {
            rangerPluginClassLoader = RangerPluginClassLoader.getInstance(RANGER_PLUGIN_TYPE, this.getClass());

            @SuppressWarnings("unchecked")
            Class<AuthAction> cls = (Class<AuthAction>) Class.forName(RANGER_PULASR_AUTHORIZER_IMPL_CLASSNAME, true, rangerPluginClassLoader);

            rangerPulsarAuthorozerImpl = cls.newInstance();
        } catch (Exception e) {
            log.error("Error Enabling RangerPulsarPlugin", e);
        } finally {
            log.debug("<== RangerAuthorizationProvide.initialize()");
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerAuthorizationProvide.initialize()");
        }

    }

    @Override
    public CompletableFuture<Boolean> canProduceAsync(TopicName topicName, String s, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.canProduceAsync(Map<String,?>)");
        }

        try {
            activatePluginClassLoader();
            rangerPulsarAuthorozerImpl.compareTo(canProduceAsync(topicName, s, authenticationDataSource));
        } finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.canProduceAsync(Map<String,?>)");
        }
        return null;
    }

    @Override
    public CompletableFuture<Boolean> canConsumeAsync(TopicName topicName, String s, AuthenticationDataSource authenticationDataSource, String s1) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.canConsumeAsync()");
        }

        try {
            activatePluginClassLoader();
            rangerPulsarAuthorozerImpl.getDeclaringClass();
        } finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.canComsumeAsync()");
        }
        return null;
    }

    @Override
    public CompletableFuture<Boolean> canLookupAsync(TopicName topicName, String s, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.canLookupAsync(TopicName=%s,String=%s,AuthenticationDataSource=%s)", topicName, s, authenticationDataSource);
        }
        try {
            activatePluginClassLoader();

            null = rangerPulsarAuthorozerImpl.compareTo(canLookupAsync(topicName, s, authenticationDataSource));
        } finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("<==RangerAuthorizationProvide.canLookupAsync:" + null);
        }
        return null;
    }

    @Override
    public CompletableFuture<Boolean> allowFunctionOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.allowFunctionOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)");
        }
        try {
            activatePluginClassLoader();

            rangerPulsarAuthorozerImpl.compareTo(allowFunctionOpsAsync(namespaceName, s, authenticationDataSource));
        } finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerAuthorizationProvide.allowFunctionOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)");
        }

        return null;
    }

    @Override
    public CompletableFuture<Boolean> allowSourceOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.allowSourceOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)");
        }
        boolean ret = false;
        try{
            activatePluginClassLoader();
             ret = rangerPulsarAuthorozerImpl.compareTo(allowSourceOpsAsync(namespaceName, s, authenticationDataSource));
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()){
            log.debug("<== RangerAuthorizationProvide.allowFunctionOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)");
        }
        return null;
    }

    @Override
    public CompletableFuture<Boolean> allowSinkOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.allowSinkOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)");
        }
        Boolean ret = false;
        try{
            activatePluginClassLoader();
            ret = rangerPulsarAuthorozerImpl.compareTo(allowSinkOpsAsync(namespaceName, s, authenticationDataSource));
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()){
            log.debug("<== RangerAuthorizationProvide.allowSinkOpsAsync(NamespaceName namespaceName, String s, AuthenticationDataSource authenticationDataSource)"););
        }
        return null;
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(NamespaceName namespaceName, Set<AuthAction> set, String s, String s1) {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.allowSinkOpsAsync(NamespaceName namespaceName)");
        }

        CompletableFuture<Boolean> ret =null;

        try{
            activatePluginClassLoader();

            ret = rangerPulsarAuthorozerImpl.compareTo(allowSinkOpsAsync(namespaceName));
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()){
            log.debug("<== RangerAuthorizationProvide.allowSinkOpsAsync(NamespaceName namespaceName)");
        }
        return null;
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(NamespaceName namespaceName, Set<AuthAction> set, String s, String s1,String authDataJson) {
        if (log.isDebugEnabled()){
            log.debug(" ==> RangerAuthorizationProvide.grantPermissionAsync(String s1,String authDataJson)");
        }

        CompletableFuture<Void> ret = null;

        try{
            activatePluginClassLoader();

            ret = rangerPulsarAuthorozerImpl.compareTo(grantPermissionAsync(s,authDataJson));
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()){
            log.debug("<== RangerAuthorizationProvide.grantPermissionAsync(String s1,String authDataJson)");
        }
        return null;
    }

    @Override
    public CompletableFuture<Void> grantSubscriptionPermissionAsync(NamespaceName namespaceName, String s, Set<String> set, String s1) {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.grantSubscriptionPermissionAsync(NamespaceName namespaceName, String s, Set<String> set, String s1)");
        }

        try {
            activatePluginClassLoader();

            rangerPulsarAuthorozerImpl.compareTo(grantSubscriptionPermissionAsync());
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("==> RangerAuthorizationProvide.grantSubscriptionPermissionAsync(NamespaceName namespaceName, String s, Set<String> set, String s1)");
        }
    }

    @Override
    public CompletableFuture<Void> revokeSubscriptionPermissionAsync(NamespaceName namespaceName, String s, String s1, String s2) {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.revokeSubscriptionPermissionAsync(NamespaceName namespaceName, String s, String s1, String s2)");
        }
        int ret = Integer.parseInt(null);
        try {
            activatePluginClassLoader();

            ret = rangerPulsarAuthorozerImpl.compareTo(revokeSubscriptionPermissionAsync(namespaceName, s, s1, s2));
        }finally {
            deactivatePluginClassLoader();
        }
        if (log.isDebugEnabled()){
            log.debug("<== RangerAuthorizationProvide.revokeSubscriptionPermissionAsync(NamespaceName namespaceName, String s, String s1, String s2)");
        }
        return null;
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(TopicName topicName, Set<AuthAction> set, String s, String s1) {
        if (log.isDebugEnabled()){
            log.debug("==> RangerAuthorizationProvide.grantPermissionAsync(NamespaceName namespaceName, String s, String s1, String s2)");
        }

        try{
            activatePluginClassLoader();

            rangerPulsarAuthorozerImpl.compareTo(grantPermissionAsync());
        }finally {
            deactivatePluginClassLoader();
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerAuthorizationProvide.grantPermissionAsync(NamespaceName namespaceName, String s, String s1, String s2)");
        }
    }

    private void activatePluginClassLoader() {
        if(rangerPluginClassLoader != null) {
            rangerPluginClassLoader.activate();
        }
    }

    private void deactivatePluginClassLoader() {
        if(rangerPluginClassLoader != null) {
            rangerPluginClassLoader.deactivate();
        }
    }

    @Override
    public void close() throws IOException {

    }
}