package org.apache.ranger.authorization.pulsar.authorizer;

import org.apache.bookkeeper.util.StringUtils;
import org.apache.pulsar.broker.PulsarServerException;
import org.apache.pulsar.broker.ServiceConfiguration;
import org.apache.pulsar.broker.authentication.AuthenticationDataSource;
import org.apache.pulsar.broker.authorization.AuthorizationProvider;
import org.apache.pulsar.broker.resources.PulsarResources;
import org.apache.pulsar.broker.web.RestException;
import org.apache.pulsar.common.naming.NamespaceName;
import org.apache.pulsar.common.naming.TopicName;
import org.apache.pulsar.common.policies.data.*;
import org.apache.pulsar.common.util.FutureUtil;
import org.apache.ranger.plugin.service.RangerBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class RangerServicePulsar extends RangerBaseService {
    private static final Logger log = LoggerFactory.getLogger(RangerServicePulsar.class);

    private AuthorizationProvider provider;
    private final ServiceConfiguration conf;

    public AuthorizationService(ServiceConfiguration conf, PulsarResources pulsarResources) throws PulsarServerException {
        this.conf = conf;
        try {
            final String providerClassname = conf.getAuthorizationProvider();
            if (StringUtils.isNotBlank(providerClassname)) {
                provider = (AuthorizationProvider) Class.forName(providerClassname)
                        .getDeclaredConstructor().newInstance();
                provider.initialize(conf, pulsarResources);
                log.info("{} has been loaded.", providerClassname);
            } else {
                throw new PulsarServerException("No authorization providers are present.");
            }
        } catch (PulsarServerException e) {
            throw e;
        } catch (Throwable e) {
            throw new PulsarServerException("Failed to load an authorization provider.", e);
        }
    }

    public AuthorizationProvider getProvider() {
        return provider;
    }

    @Override
    public CompletableFuture<Boolean> isSuperUser(String user, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.isSuperUser(" + user + ")");
        }
        if (provider != null) {
            return provider.isSuperUser(user, authenticationData, conf);
        }
        if (configs != null) {
            log.debug("<== RangerServicePulsar.isSuperUser(" + user + ")ret=" + authenticationData);
        }
        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Boolean> isTenantAdmin(String tenant, String role, TenantInfo tenantInfo, AuthenticationDataSource authenticationDataSource) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.isTenantAdmin(" + tenant + ")");
        }
        if (provider != null) {
            return provider.isTenantAdmin(tenant, role, tenantInfo, authenticationDataSource);
        }
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.isTenantAdmin(" + tenant + ")");
        }
        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(NamespaceName namespace, Set<AuthAction> actions, String role, String authDataJson) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.grantPermissionAsync(" + namespace + ")");
        }

        if (provider != null) {
            return provider.grantPermissionAsync(namespace, actions, role, authDataJson);
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.grantPermissionAsync(" + namespace + ")");
        }

        return FutureUtil.failedFuture(new IllegalStateException(("No authorization provider configured")));

    }

    @Override
    public CompletableFuture<Void> grantSubscriptionPermissionAsync(NamespaceName namespace, String subscriptionName, Set<String> roles, String authDataJson) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.grantSubscriptionPermissionAsync(" + roles + ")");
        }

        if (provider != null) {
            return provider.grantPermissionAsync(namespace, subscriptionName, roles, authDataJson);
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.grantSubscriptionPermissionAsync(" + roles + ")");
        }

        return FutureUtil.failedFuture(new IllegalStateException(("No authorization provider configured")));
    }

    @Override
    public CompletableFuture<Void> revokeSubscriptionPermissionAsync(NamespaceName namespace, String subscriptionName, String role, String authDataJson) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.revokeSubscriptionPermissionAsync(" + role + ")");
        }

        if (provider != null) {
            return provider.revokeSubscriptionPermissionAsync(namespace, subscriptionName, role, authDataJson);
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.revokeSubscriptionPermissionAsync(" + role + ")");
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Void> grantPermissionAsync(TopicName topicname, Set<AuthAction> actions, String role, String authDataJson) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.grantPermissionAsync(" + topicname + ")");
        }

        if (provider != null) {
            return provider.grantPermissionAsync(topicname, actions, role, authDataJson);
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.grantPermissionAsync(" + topicname + ")");
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Boolean> canProduceAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canProduceAsync(" + role + "):tpoicName=" + topicName);
        }

        if (!this.conf.isAuthorizationEnabled()) {
            return CompletableFuture.completedFuture(true);
        }
        if (provider != null) {
            return provider.isSuperUser(role, authenticationData, conf).thenCombineAsync(isSuperUser -> {
                if (isSuperUser) {
                    return CompletableFuture.completedFuture(true);
                } else {
                    return provider.canProduceAsync(topicName, role, authenticationData);
                }
            });
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canProduceAsync(" + role + "):topicName=" + topicName);
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Boolean> canConsumeAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData, String subscription) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canConsumeAsync(" + role + "):tpoicName=" + topicName);
        }

        if (!this.conf.isAuthorizationEnabled()) {
            return CompletableFuture.completedFuture(true);
        }
        if (provider != null) {
            return provider.isSuperUser(role, authenticationData, conf).thenCombineAsync(isSuperUser -> {
                if (isSuperUser) {
                    return CompletableFuture.completedFuture(true);
                } else {
                    return provider.canConsumeAsync(topicName, role, authenticationData, subscription);
                }
            });
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canConsumeAsync(" + role + "):topicName=" + topicName);
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public boolean canProduce(TopicName topicName, String role, AuthenticationDataSource authenticationData) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canProduce(" + role + ")");
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canProduce(" + role + ")");
        }

    }

    @Override
    public boolean canConsume(TopicName topicName, String role, AuthenticationDataSource authenticationData, String subscription) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canConsume(" + role + ")");
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canConsume(" + role + ")");
        }
    }

    @Override
    public boolean canLookup(TopicName topicName, String role, AuthenticationDataSource authenticationData) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canLookup(" + authenticationData + ")");
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canLookup(" + authenticationData + ")");
        }
    }

    @Override
    public CompletableFuture<Boolean> canLookupAsync(TopicName topicName, String role, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.canLookupAsync(" + role + "):topicName=" + topicName);
        }

        if (!this.conf.isAuthorizationEnabled()) {
            return CompletableFuture.completedFuture(true);
        }
        if (provider != null) {
            return provider.isSuperUser(role, authenticationData, conf).thenComposeAsync(isSuperUser -> {
                if (isSuperUser) {
                    return CompletableFuture.completedFuture(true);
                } else {
                    return provider.canLookupAsync(topicName, role, authenticationData);
                }
            });
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.canLookupAsync(" + role + "):topicName=" + topicName);
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured"));
    }

    @Override
    public CompletableFuture<Boolean> allowFunctionOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowFunctionOpsAsync()");
        }

        return provider.allowFunctionOpsAsync(namespaceName, role, authenticationData);

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.allowFunctionOpsAsync()");
        }
    }

    @Override
    public CompletableFuture<Boolean> allowSourceOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowSourceOpsAsync()");
        }

        return provider.allowSourceOpsAsync(namespaceName, role, authenticationData);

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.allowSourceOpsAsync()");
        }
    }

    @Override
    public CompletableFuture<Boolean> allowSinkOpsAsync(NamespaceName namespaceName, String role, AuthenticationDataSource authenticationData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowSinkOpsAsync()");
        }

        return provider.allowSinkOpsAsync(namespaceName, role, authenticationData);

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.allowSinkOpsAsync()");
        }
    }

    @Override
    private static void validateOriginalPrincipal(Set<String> proxyRoles, String authenticatedPrincipal, String originalPrincipal) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.validateOriginalPrincipal()");
        }

        if (proxyRoles.contains(authenticatedPrincipal)) {
            if (StringUtils.isBlank(originalPrincipal)) {
                log.warn(log.warn("Original principal empty in request authenticated as {}", authenticatedPrincipal);
                throw new RestException(Response.Status.UNAUTHORIZED, "Original principal cannot be empty if the request is via proxy.");)
                ;
            }
            if (proxyRoles.contains(originalPrincipal)) {
                log.warn("Original principal {} cannot be a proxy role ({})", originalPrincipal, proxyRoles);
                throw new RestException(Response.Status.UNAUTHORIZED, "Original principal cannot be a proxy role");
            }

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.validateOriginalPrincipal()");
            }
        }

        @Override
        private boolean isProxyRole (String role){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.isProxyRole()");
            }

            return role != null && conf.getProxyRoles().contains(role);

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.isProxyRole()");
            }
        }

        @Override
        public CompletableFuture<Boolean> allowTenantOperationAsync (String conf, TenantOperation operation, String
        role, AuthenticationDataSource authData){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.allowTenantOperationAsync()");
            }

            if (!this.conf.isAuthorizationEnabled()) {
                return CompletableFuture.completedFuture(true);
            }

            if (provider != null) {
                return provider.allowTenantOperationAsync(tenantName, role, operation, authData);
            }

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.allowTenantOperationAsync()");
            }

            return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured for" + "allowTenantOperationAsync"));
        }

        @Override
        public boolean allowTenantOperation (String conf, TenantOperation operation, String originalRole, String
        role, AuthenticationDataSource authData){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.allowTenantOperation()");
            }

            validateOriginalPrincipal(conf.getProxyRoles(), role, originalRole);
            if (isProxyRole(role)) {
                CompletableFuture<Boolean> isRoleAuthorizedFuture = allowTenantOperationAsync(
                        tenantName, operation, role, authData);
                CompletableFuture<Boolean> isOriginalAuthorizedFuture = allowTenantOperationAsync(
                        tenantName, operation, originalRole, authData);
                return isRoleAuthorizedFuture.thenCombine(isOriginalAuthorizedFuture,
                        (isRoleAuthorized, isOriginalAuthorized) -> isRoleAuthorized && isOriginalAuthorized);
            } else {
                return allowTenantOperationAsync(tenantName, operation, role, authData);
            }

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.allowTenantOperation()");
            }
        }

        @Override
        public CompletableFuture<Boolean> allowNamespaceOperationAsync (String tenantName, TenantOperation
        operation, String role, AuthenticationDataSource authData){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.allowNamespaceOperationAsync()");
            }

            if (!this.conf.isAuthorizationEnabled()) {
                return CompletableFuture.completedFuture(true);
            }
            if (provider != null) {
                return provider.allowTenantOperationAsync(tenantName, role, operation, authData);
            }

            try {
                return allowTenantOperationAsync(
                        tenantName, operation, originalRole, role, authData).get();
            } catch (InterruptedException e) {
                throw new RestException(e);
            } catch (ExecutionException e) {
                throw new RestException(e.getCause());
            }

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.allowNamespaceOperationAsync()");
            }

            return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured for" + "allowTenantOperationAsync"))
        }

        @Override
        public CompletableFuture<Boolean> allowNamespacePolicyOperationAsync (NamespaceName namespaceName, PolicyName
        policy, PolicyOperation operation, String role, AuthenticationDataSource authData){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.allowNamespacePolicyOperationAsync()");
            }

            validateOriginalPrincipal(conf.getProxyRoles(), role, originalRole);
            if (isProxyRole(role)) {
                CompletableFuture<Boolean> isRoleAuthorizedFuture = allowNamespaceOperationAsync(
                        namespaceName, operation, role, authData);
                CompletableFuture<Boolean> isOriginalAuthorizedFuture = allowNamespaceOperationAsync(
                        namespaceName, operation, originalRole, authData);
                return isRoleAuthorizedFuture.thenCombine(isOriginalAuthorizedFuture, (isRoleAuthorized, isOriginalAuthorized) -> isRoleAuthorized && isOriginalAuthorized);
            } else {
                return allowNamespaceOperationAsync(namespaceName, operation, role, authData);
            }

            try {
                return allowNamespaceOperationAsync(namespaceName, operation, originalRole, role, authData).get();
            } catch (InterruptedException e) {
                throw new RestException(e);
            } catch (ExecutionException e) {
                throw new RestException(e.getCause());
            }

            if (log.isDebugEnabled()) {
                log.debug("<== RangerServicePulsar.allowNamespacePolicyOperationAsync()");
            }
        }

        @Override
        public CompletableFuture<Boolean> allowNamespacePolicyOperationAsync (NamespaceName namespaceName, PolicyName
        policy, PolicyOperation operation, String originalRole, String role, AuthenticationDataSource authData){
            if (log.isDebugEnabled()) {
                log.debug("==> RangerServicePulsar.allowNamespacePolicyOperationAsync()");
            }

            if (!this.conf.isAuthorizationEnabled()) {
                return CompletableFuture.completedFuture(true);
            }
            if (provider != null) {
                return provider.allowNamespacePolicyOperationAsync(namespaceName, policy, operation, role, authData);
            }
            return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured for " + "allowNamespacePolicyOperationAsync"));
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.allowNamespacePolicyOperationAsync()");
        }
    }

    @Override
    public boolean allowNamespacePolicyOperation(NamespaceName namespaceName, PolicyName policy, PolicyOperation operation, String originalRole, String role, AuthenticationDataSource authData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowNamespacePolicyOperationAsync()");
        }

        try {
            return allowTopicPolicyOperationAsync(topicName, policy, operation, originalRole, role, authData).get();
        } catch (InterruptedException e) {
            throw new RestException(e);
        } catch (ExecutionException e) {
            throw new RestException(e.getCause());
        }

        if (log.isDebugEnabled()) {
            log.debug("<== RangerServicePulsar.allowNamespacePolicyOperationAsync()");
        }
    }

    @Override
    public CompletableFuture<Boolean> allowTopicPolicyOperationAsync(TopicName topicName, PolicyName policy, PolicyOperation operation, String role, AuthenticationDataSource authData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicPolicyOperationAsync()");
        }

        if (log.isDebugEnabled()) {
            log.debug("Check if role {} is allowed to execute topic operation {} on topic {}",
                    role, operation, topicName);
        }
        if (!this.conf.isAuthorizationEnabled()) {
            return CompletableFuture.completedFuture(true);
        }

        if (provider != null) {
            CompletableFuture<Boolean> allowFuture =
                    provider.allowTopicOperationAsync(topicName, role, operation, authData);
            if (log.isDebugEnabled()) {
                return allowFuture.whenComplete((allowed, exception) -> {
                    if (exception == null) {
                        if (allowed) {
                            log.debug("Topic operation {} on topic {} is allowed: role = {}",
                                    operation, topicName, role);
                        } else {
                            log.debug("Topic operation {} on topic {} is NOT allowed: role = {}",
                                    operation, topicName, role);
                        }
                    } else {
                        log.debug("Failed to check if topic operation {} on topic {} is allowed:"
                                        + " role = {}",
                                operation, topicName, role, exception);
                    }
                });
            } else {
                return allowFuture;
            }
        }

        return FutureUtil.failedFuture(new IllegalStateException("No authorization provider configured for " +
                "allowTopicOperationAsync"));

        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicPolicyOperationAsync()");
        }
    }

    @Override
    public CompletableFuture<Boolean> allowTopicPolicyOperationAsync(TopicName topicName, PolicyName policy, PolicyOperation operation, String originalRole, String role, AuthenticationDataSource authData) {
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicPolicyOperationAsync()");
        }

        validateOriginalPrincipal(conf.getProxyRoles(), role, originalRole);
        if (isProxyRole(role)) {
            CompletableFuture<Boolean> isRoleAuthorizedFuture = allowTopicOperationAsync(
                    topicName, operation, role, authData);
            CompletableFuture<Boolean> isOriginalAuthorizedFuture = allowTopicOperationAsync(
                    topicName, operation, originalRole, authData);
            return isRoleAuthorizedFuture.thenCombine(isOriginalAuthorizedFuture,
                    (isRoleAuthorized, isOriginalAuthorized) -> isRoleAuthorized && isOriginalAuthorized);
        } else {
            return allowTopicOperationAsync(topicName, operation, role, authData);
        }

        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicPolicyOperationAsync()");
        }
    }

    @Override
    public Boolean allowTopicOperation(TopicName topicName,TopicOperation operation,String originalRole,String role,AuthenticationDataSource authData){
        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicOperation()");
        }

        try {
            return allowTopicOperationAsync(topicName, operation, originalRole, role, authData).get();
        } catch (InterruptedException e) {
            throw new RestException(e);
        } catch (ExecutionException e) {
            throw new RestException(e.getCause());
        }

        if (log.isDebugEnabled()) {
            log.debug("==> RangerServicePulsar.allowTopicOperation()");
        }
    }
}