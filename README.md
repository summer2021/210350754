项目名称：实现 Apache Pulsar 与 Apache Ranger 的功能集成

项目综述：
Apache Pulsar 主要是一个消息系统，Apache ranger 是一个集中式的安全管理框架。
项目是要基于 Apache ranger 为 Apache pulsar 实现一个新的 Apache Pulsar 授权框
架。

项目实现：

Apache Ranger实现代码存放于文件plugin-pulsar/src中

Apache Pulsar实现代码存放于文件pulsar-broker-common/main中


项目后期任务：

完善代码、修改API、学习Ranger、Pulsar Connectors\Adapters原理

项目结项：

已实现 Apache Ranger 对 Apache Pulsar 的支持功能
代码符合规范要求
对部分工程添加集成测试，来保证部分功能的正常使用
